/*  Trabalho Sistemas Operacionais Multi Threading

    Autores: Matheus Baldas, Pedro Carvalho, Rodolfo

    Criado em: Outubro de 2018

*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>

void *exec(void *arg){
    int *valor;
    valor = arg;

    printf("Eu sou a thread: %i || meu id via pthread_self eh: %d || meu id via gettid eh: %d\n", *valor, pthread_self(),gettid());

}

int main()
{
    int num_t,i;
    printf("Digite o valor de threads que deseja ter: \n");
    scanf("%d",&num_t);

    pthread_t threads[num_t];
    int arg[num_t];

    for(i=0;i<num_t;i++){
        arg[i] = i+1;
        pthread_create(&threads[i],NULL,exec, (void *)&arg[i]);
    }

    for(i=0;i<num_t;i++){
        pthread_join(threads[i],NULL);
    }

    printf("\n");

    return 0;
}
